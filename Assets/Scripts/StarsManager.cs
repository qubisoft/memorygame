﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StarsManager : MonoBehaviour
{
    public Image[] stars;
    public Text tMessage;

    private void Start()
    {
        StartCoroutine(pauseStar());
    }

    IEnumerator pauseStar()
    {
        switch (StaticClass.iStars)
        {
            default:
            case (1):
                tMessage.text = "Nice! You've made this in " + StaticClass.iMoves.ToString() + " moves. Please try again to get more stars!";
                break;
            case (2):
                tMessage.text = "Well done! You've made this in " + StaticClass.iMoves.ToString() + " moves. Please try again to get three stars next time!" +
                        "";
                break;
            case (3):
                tMessage.text = "Excellent! You've made this in only " + StaticClass.iMoves.ToString() + " moves!";
                break;
        }

        //fill won stars with image
        for (int i = 0; i < StaticClass.iStars; i++)
        {
            stars[i].GetComponent<Animator>().SetTrigger("win" + (i+1));
            stars[i].GetComponent<ParticleSystem>().Play();
            yield return new WaitForSeconds(1f);
        }
    }

}
    

