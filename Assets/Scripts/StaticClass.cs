﻿public static class StaticClass
{
    // Variables for stars navigation
    public static int iStars; 
    public static int iMoves; 

    // Variables for audio navigation
    public static int iSoundsInit; 
    public static bool bSoundOn;  
    public static bool bMusicOn;

    // Variables for scene navigation
    public static int iLevelNav;

}