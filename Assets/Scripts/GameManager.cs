﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public Sprite[] buttonFace;
    public Sprite buttonBack;
    public GameObject[] buttons;
    public Text matchText;
    public Text movesText;
    public AudioSource clickSound;
    public AudioSource winSound;

    private int _iMatches;              
    private int iMatches;
    private int iCompare;

    private int[] buttonFacesUsed;      //0 - not used, 1 - used once, 2 - used twice
    private int[] buttonInfo;           //array.length = 2*iMatches, connection between button number and face number
    private int[] iButtonCompare;
    List<int> numbersToChooseFrom = new List<int>();

    private bool check;
    private int iFace;
    private int iButton;

    private void Start()
    {
        iMatches = buttonFace.Length;
         _iMatches = iMatches;            

        StaticClass.iMoves = 0;
        iCompare = 0;

        buttonFacesUsed = new int[iMatches];
        buttonInfo = new int[2 * iMatches];
        iButtonCompare = new int[2];
        for (int i = 0; i < iMatches; i++)
            numbersToChooseFrom.Add(i);
        initializeButtons();
    }

    private void Update()
    {
        if (iCompare == 2)
        {
            buttonComparison(iButtonCompare[0], iButtonCompare[1]);
        }
    }

    public void flipButton(GameObject button)
    {
        clickSound.Play();
        iButton = System.Array.IndexOf(buttons, button);
        button.GetComponent<Image>().sprite = buttonFace[buttonInfo[iButton]];
        iButtonCompare[iCompare] = iButton;   
        if (iCompare == 0 || (iCompare == 1 && !(iButtonCompare[0] == iButtonCompare[1])))
            iCompare++;
    }

    void initializeButtons()
    {
        // Prepare faces info  
        for (int iFace = 0; iFace < iMatches; iFace++)
        {
            buttonFacesUsed[iFace] = 0;
        }

        iButtonCompare[0] = -99;
        iButtonCompare[1] = -99;

        // Prepare button info
        for (int iButton = 0; iButton < iMatches*2; iButton++)
        {
            buttons[iButton].GetComponent<Image>().sprite = buttonBack;
            check = true;
            while (check)
            {
                iFace = numbersToChooseFrom[Random.Range(0,numbersToChooseFrom.Count)];
                
                if (buttonFacesUsed[iFace] == 0 || buttonFacesUsed[iFace] == 1)
                {
                    buttonInfo[iButton] = iFace;
                    buttonFacesUsed[iFace]++;
                    check = false;

                    if (buttonFacesUsed[iFace] == 2)
                    {
                        numbersToChooseFrom.Remove(iFace);
                    }
                }
            }

        }

       
    }

    private void buttonComparison(int i1, int i2)
    {
        StaticClass.iMoves++;
        movesText.text = "Moves: " + StaticClass.iMoves;

        if (buttonInfo[i1] == buttonInfo[i2])
        {
            winSound.Play();

            buttons[i1].GetComponent<Animator>().SetTrigger("Active");
            buttons[i2].GetComponent<Animator>().SetTrigger("Active");

            _iMatches--;
            matchText.text = "Matches left: " + _iMatches;

            if (_iMatches == 0)
            {
                if (StaticClass.iMoves < 2 * iMatches)
                    StaticClass.iStars = 3;
                else if (StaticClass.iMoves < 3 * iMatches)
                    StaticClass.iStars = 2;
                else
                    StaticClass.iStars = 1;

                SceneManager.LoadScene("Results");
            }
        }
        else
        {
            _ = StartCoroutine(hideButtons(i1,i2));
        }

        iCompare = 0;

        iButtonCompare[0] = 0;
        iButtonCompare[1] = 0;
    }

    IEnumerator hideButtons(int i1, int i2)
    {
        yield return new WaitForSeconds(0.5f);
        buttons[i1].GetComponent<Image>().sprite = buttonBack;
        buttons[i2].GetComponent<Image>().sprite = buttonBack;
    }

}
