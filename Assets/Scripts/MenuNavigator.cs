﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuNavigator : MonoBehaviour
{
    public void triggerMenuNavigator(int i)
    {
        switch(i)
        {
        default:
        case(0):
            SceneManager.LoadScene("Menu");
            break;
        case(1):
            SceneManager.LoadScene("lEasy");
            StaticClass.iLevelNav = 1;
            break;
        case(2):
            SceneManager.LoadScene("lMedium");
            StaticClass.iLevelNav = 2;
            break;
        case(3):
            SceneManager.LoadScene("lHard");
            StaticClass.iLevelNav = 3;
            break;
        case (10):
            SceneManager.LoadScene("Info");
            break;
        case (99):
            triggerMenuNavigator(StaticClass.iLevelNav);
            break;
        case (100):
            Application.Quit();
            break;
        }
    }
}
