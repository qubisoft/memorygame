﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicNavigator : MonoBehaviour
{
    public Image soundON;
    public Image soundOFF;
    public AudioSource[] soundSource;
    public Image musicON;
    public Image musicOFF;
    public AudioSource[] musicSource;

    private void Start()
    {
        if (StaticClass.iSoundsInit == 0)
        {
            StaticClass.bMusicOn = true;
            StaticClass.bSoundOn = true;
            StaticClass.iSoundsInit = 1;
        }

        setRightSounds(ref StaticClass.bSoundOn, ref soundON, ref soundOFF, ref soundSource);
        setRightSounds(ref StaticClass.bMusicOn, ref musicON, ref musicOFF, ref musicSource);
    }

    private void setRightSounds(ref bool bSoundON, ref Image iSoundON, ref Image iSoundOFF, ref AudioSource[] audioSource)
    {
        if (bSoundON)
        {
            iSoundON.enabled = true;
            iSoundOFF.enabled = false;
            for (int i = 0; i < audioSource.Length; i++)
                audioSource[i].mute = false;
        }
        else
        {
            iSoundON.enabled = false;
            iSoundOFF.enabled = true;
            for (int i = 0; i < audioSource.Length; i++)
                audioSource[i].mute = true;
        }
    }

    private void TurnAudioOnOff(ref bool bAudioOn, ref Image AudioON, ref Image AudioOFF, ref AudioSource[] audioSources)
    {
        if (bAudioOn)
        {
            bAudioOn = false;
            AudioON.enabled = false;
            AudioOFF.enabled = true;
            for (int i = 0; i < audioSources.Length; i++)
                audioSources[i].mute = true;
        }
        else
        {
            bAudioOn = true;
            AudioON.enabled = true;
            AudioOFF.enabled = false;
            for (int i = 0; i < audioSources.Length; i++)
                audioSources[i].mute = false;
        }
    }

    public void TurnSoundOnOff()
    {
        TurnAudioOnOff(ref StaticClass.bSoundOn, ref soundON, ref soundOFF, ref soundSource);
    }

    public void TurnMusicOnOff()
    {
        TurnAudioOnOff(ref StaticClass.bMusicOn, ref musicON, ref musicOFF, ref musicSource);
    }
}
